package com.example.generacion

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        evaluar_Click()
    }

    fun evaluar_Click() {


        btnVerificar.setOnClickListener {

            var respuesta = ""
            if (edtEdad.text.toString().isEmpty()) {
                imagen.setImageDrawable(null)
                tvResultado.text = respuesta
                Toast.makeText(this, "No puede ingresar edad vacia", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val edad = (edtEdad.text.toString()).toInt()
            when (edad) {
                in (1994..2010) -> {
                    tvResultado.setText("Generación Z, la población de su generación es 7 800 0000")
                    imagen.setImageResource(R.drawable.irreverencia)
                }
                in (1981..1993) -> {
                    tvResultado.setText("Generación Y, la población de su generación es 7 200 0000")
                    imagen.setImageResource(R.drawable.frustacion)
                }
                in (1969..1980) -> {
                    tvResultado.setText("Generación X, la población de su generación es 9 300 0000")
                    imagen.setImageResource(R.drawable.exito)
                }
                in (1949..1968) -> {
                    tvResultado.setText("Baby Boom, la población de su generación es 12 200 0000")
                    imagen.setImageResource(R.drawable.ambicion)
                }
                in (1930..1948) -> {
                    tvResultado.setText("Silent Generation, la población de su generación es 6 300 0000")
                    imagen.setImageResource(R.drawable.austeridad)
                }
                else -> {
                    tvResultado.text = "No esta definido ese año en alguna generación"
                }
            }
        }
    }
}